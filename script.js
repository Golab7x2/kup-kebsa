var stuffedStudents = 0;
var hungryStudents = 20;
const GROWTH = 1.2; // +20%
var timeLeft = 5;
const LEVEL_TIME = 6;
var semester = 1;
var score = 0;
var timer;
var progressBarWidth = 0;

$("#buy").click(function() {
    if(stuffedStudents == 0) timer = setInterval(countdown, 1000);

    stuffedStudents++;
    $("#stuffed-students").text(stuffedStudents);

    countIndex();

    score += semester;
    $(".score").text(score);

});

function countdown() {
    
    timeLeft--;
    $("#time").text(timeLeft);

    if(timeLeft == 0) {
        stuffedStudents -= hungryStudents;
        $("#stuffed-students").text(stuffedStudents);

        hungryStudents = Math.round(hungryStudents * GROWTH);
        $("#hungry-students").text(hungryStudents);

        semester++;
        $(".semester").text(semester);

        timeLeft = LEVEL_TIME;
    }

    countIndex();

    if(stuffedStudents < 0) {

        clearInterval(timer);
        $("#buy").attr("disabled", true);
        $("#game-over").show("slow");
    }

}

function countIndex() {

    var index = stuffedStudents / hungryStudents * 1000;
    if(index > 1000) index = 1000;
    if(index < 0) index = 0;
    $("#stuffed-bar").width(index);
    $("#percent").text(Math.round(index/10) + "%");

}

$("#reset").click(function() {
    
    stuffedStudents = 0;
    $("#stuffed-students").text(stuffedStudents);
    hungryStudents = 20;
    $("#hungry-students").text(hungryStudents);
    timeLeft = 5;
    $("#time").text(timeLeft);
    semester = 1;
    $(".semester").text(semester);
    score = 0;
    $(".score").text(score);

    $("#buy").attr("disabled", false);
    $("#game-over").hide();
});

$("#info-mark").hover(
    function() {
        $("#info").show()
    }, 
    function() {
        $("#info").hide();
    });